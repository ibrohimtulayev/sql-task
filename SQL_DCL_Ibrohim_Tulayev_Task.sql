CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE your_database TO rentaluser;

GRANT SELECT ON TABLE customer TO rentaluser;

CREATE GROUP rental;
GRANT rental TO rentaluser;

GRANT INSERT, UPDATE ON TABLE rental TO rental;

INSERT INTO rental (column1, column2, ...) VALUES (value1, value2, ...);
UPDATE rental SET column1 = value1 WHERE condition;

REVOKE INSERT ON TABLE rental FROM rental;

INSERT INTO rental (column1, column2, ...) VALUES (value1, value2, ...);

CREATE ROLE client_first_name_last_name;
GRANT SELECT ON TABLE rental, payment TO client_first_name_last_name;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
   GRANT SELECT ON TABLE rental, payment TO client_first_name_last_name;

SET ROLE client_first_name_last_name;
SELECT * FROM rental WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = '{first_name}' AND last_name = '{last_name}');
SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = '{first_name}' AND last_name = '{last_name}');
RESET ROLE;
